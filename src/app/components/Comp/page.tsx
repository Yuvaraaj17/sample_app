import React, { useState } from "react"

export default function Comp(){
    let value = process.env.name

    if (value === undefined){
        value = 'No value right now'
    }
    console.log(value)
    return(
        <h1>{value}</h1>
    )
}