# @echo off

echo starting execution
node updateConfig.js name="one"
echo script executed

# @REM ping /n 5 localhost

# @REM node updateConfig.js name="one" && (
# @REM     ping /n 5 localhost
# @REM     node updateConfig.js name="two"
# @REM     ping /n 5 localhost
# @REM     node updateConfig.js name="three"
# @REM     ping /n 5 localhost
# @REM     node updateConfig.js name="four"
# @REM     ping /n 5 localhost
# @REM     node updateConfig.js reset

# @REM )