// updateConfig.js

const fs = require('fs');

// Read command line arguments
const args = process.argv.slice(2);
if (args == 'reset') {
    fs.writeFileSync('./next.config.js', `module.exports = {}`);
}

else {
    const [variable, value] = args[0].split('=');
    console.log(variable)
    console.log(value)

    // Read the next.config.js file
    const config = require('./next.config.js');
    console.log(config)

    // // Update the environment variable
    config.env = { name: value };
    console.log(JSON.stringify(config, null, 2))

    // // Write the updated configuration back to next.config.js
    fs.writeFileSync('./next.config.js', `module.exports = ${JSON.stringify(config, null, 2)}`);
}

// Extract environment variable and value from command line arguments

